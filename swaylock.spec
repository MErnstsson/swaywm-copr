Name:           swaylock
Version:        1.4
Release:        0.1%{?dist}
Summary:        swaylock is a screen locking utility for Wayland compositors.
Group:          User Interface/X
License:        MIT
URL:            https://github.com/swaywm/%{name}
Source0:        %{url}/archive/%{version}.tar.gz

BuildRequires:  git
BuildRequires:  meson
BuildRequires:  pkgconfig(wayland-protocols)
BuildRequires:  wayland-devel
BuildRequires:  scdoc
BuildRequires:  gdk-pixbuf2

Requires:  libxkbcommon
Requires:  pam
Requires:  cairo
Requires:  wayland
Requires:  wayland-protocols
Requires:  gdk-pixbuf2

%description
swaylock is a screen locking utility for Wayland compositors.

%prep
%autosetup

%build
%meson
%meson_build

%install
%meson_install

%files
%license LICENSE
%doc README.md
%{_mandir}/man1/swaylock*.1*
%{_bindir}/swayidle

%changelog
