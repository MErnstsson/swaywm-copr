Name:           swayidle
Version:        1.5
Release:        0.1%{?dist}
Summary:        i3-compatible window manager for Wayland
Group:          User Interface/X
License:        MIT
URL:            https://github.com/swaywm/%{name}
Source0:        %{url}/archive/%{version}.tar.gz

BuildRequires:  gcc-c++
BuildRequires:  git
BuildRequires:  meson
BuildRequires:  pkgconfig(wayland-protocols)
BuildRequires:  pkgconfig(libsystemd)
BuildRequires:  wayland-devel
BuildRequires:  scdoc

%description
This is sway's idle management daemon, swayidle. It is compatible with any Wayland
compositor which implements the KDE idle protocol.

%prep
%autosetup

%build
%meson
%meson_build

%install
%meson_install

%files
%license LICENSE
%doc README.md
%{_mandir}/man1/swayidle*.1*
%{_bindir}/swayidle
%{_datadir}/bash-completion/completions/sway*
%exclude %{_datadir}/fish/completions/sway*
%{_datadir}/zsh/site-functions/_sway*

%changelog
