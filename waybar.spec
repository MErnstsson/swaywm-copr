# Steps for building git snapshot:
#  - uncomment and change gitdate (YYYYMMDD)
#  - set commit to desired commit hash
#  - bump Release

#global gitdate  20190519
%global commit   0.8.0

%global scommit  %(c=%{commit}; echo ${c:0:7})
%global gitrel   %{?gitdate:.%{gitdate}git%{scommit}}
%global gitver   %{?gitdate:-%{gitdate}git%{scommit}}

Name:           waybar 
Version:        0.8.0
Release:        1%{?gitrel}%{?dist}
Summary:        Highly customizable Wayland bar for Sway and Wlroots based compositors 
License:        MIT
URL:            https://github.com/Alexays/Waybar
Source0:        %{url}/archive/%{commit}.tar.gz#/%{name}-%{version}%{?gitver}.tar.gz

BuildRequires:  gcc
BuildRequires:  gcc-c++
BuildRequires:  meson >= 0.47.0
BuildRequires:  scdoc

BuildRequires:  pkgconfig(fmt) >= 5.3.0
BuildRequires:  pkgconfig(dbusmenu-gtk3-0.4)
BuildRequires:  pkgconfig(gdk-pixbuf-2.0)
BuildRequires:  pkgconfig(gio-unix-2.0)
BuildRequires:  pkgconfig(gtkmm-3.0)
BuildRequires:  pkgconfig(jsoncpp)
BuildRequires:  pkgconfig(libinput) 
BuildRequires:  pkgconfig(libmpdclient)
BuildRequires:  pkgconfig(libnl-3.0)
BuildRequires:  pkgconfig(libnl-genl-3.0)
BuildRequires:  pkgconfig(libpulse)
BuildRequires:  pkgconfig(libudev)
BuildRequires:  pkgconfig(sigc++-2.0)
BuildRequires:  pkgconfig(spdlog) >= 1.3.1
BuildRequires:  pkgconfig(wayland-client)
BuildRequires:  pkgconfig(wayland-cursor)
BuildRequires:  pkgconfig(wayland-protocols)

Recommends:     fontawesome-fonts

%description
%{summary}.

%prep
%autosetup -p 1 -n Waybar-%{commit}

%build
%meson
%meson_build

%install
%meson_install

%files
%license LICENSE
%doc README.md
%dir %{_sysconfdir}/xdg/%{name}
%config(noreplace) %{_sysconfdir}/xdg/%{name}/config
%config(noreplace) %{_sysconfdir}/xdg/%{name}/style.css
%{_bindir}/%{name}
%{_mandir}/man5/%{name}*

%changelog
* Thu Aug 29 2019 Aleksei Bavshin <alebastr89@gmail.com> - 0.8.0-1
- Update to 0.8.0

* Mon Mar 11 2019 Aleksei Bavshin <alebastr89@gmail.com> - 0.4.0-1
- Initial package
