Name:           swaybg
Version:        1.0
Release:        0.1%{?dist}
Summary:        swaybg is a wallpaper utility for Wayland compositors.
Group:          User Interface/X
License:        MIT
URL:            https://github.com/swaywm/%{name}
Source0:        %{url}/archive/%{version}.tar.gz

BuildRequires:  gcc-c++
BuildRequires:  git
BuildRequires:  meson
BuildRequires:  pkgconfig(wayland-protocols)
BuildRequires:  wayland-devel
BuildRequires:  scdoc

Requires: cairo
Requires: gdk-pixbuf2
Requires: wayland

%description
swaybg is a wallpaper utility for Wayland compositors

%prep
%autosetup

%build
%meson
%meson_build

%install
%meson_install

%files
%license LICENSE
%doc README.md
%{_bindir}/swaybg

%changelog
